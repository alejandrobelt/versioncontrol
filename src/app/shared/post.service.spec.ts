import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PostService } from './post.service';
import { HttpClientModule } from '@angular/common/http';
describe('PostService', () => {
  let postService: PostService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        HttpClientModule
      ],
      providers: [
        PostService
      ],
    });
    postService = TestBed.get(PostService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it(`should fetch posts as an Observable`, async(inject([HttpTestingController, PostService],
    (httpClient: HttpTestingController, postService: PostService) => {
      const postItem = [
          {"nro":"0.9.5","date":"2020-01-01 06:10:10"},
          {"nro":"1.0.1","date":"2021-02-01 10:16:01"},
          {"nro":"1.1.6","date":"2022-08-21 16:00:00"},
          {"nro":"1.1.7","date":"2022-10-01 07:00:10"}
        ];

      postService.getPosts()
        .subscribe((posts: any) => {
          expect(posts.length).toBe(4);
        });
      let req = httpMock.expectOne('http://c2050466.ferozo.com/public/index.php/api/disponibles/3?versiones=true');
      expect(req.request.method).toBe("GET");
      req.flush(postItem);
      httpMock.verify();
    })));
});